# Copyright <2019> Edward Verenich <verenie@clarkson.edu>
# MIT license <https://opensource.org/licenses/MIT>

from __future__ import print_function, division

import torch
import torch.nn as nn
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import time
import os
import copy
import csv
import io
import metrx as mx
import transformsx
import scipy.ndimage as nd
from PIL import Image
from sklearn.preprocessing import minmax_scale



# set the data directory for training models
DATA_DIR = 'data/xcovid'
BATCH_SIZE = 1
WORKERS = 2
IMAGE_NET_MEAN = [0.485, 0.456, 0.406]
IMAGE_NET_STD = [0.229, 0.224, 0.225]

# check for cuda and set gpu or cpu
device = torch.device("cuda:0" if torch.cuda.is_available() else 'cpu')

# data processing, adding additional sets and transforms, i.e. test corresponds to a directory
# and will create appropriate datasets
data_transforms = {
    'test': transforms.Compose([
        transforms.Resize(224),
        transforms.CenterCrop(224),
        #transformsx.GausNoise(10),
        transforms.ToTensor(),
        transforms.Normalize(IMAGE_NET_MEAN, IMAGE_NET_STD)
    ])
}

# Utility method to transform loaded image bytes to transformed input
def transform_image(image_bytes):
  mt = transforms.Compose([
    transforms.Resize(224),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(IMAGE_NET_MEAN, IMAGE_NET_STD)
  ])
  image = Image.open(io.BytesIO(image_bytes))
  print("SHAPE: ", len(image.getbands()))
  if len(image.getbands()) != 3:
      image = image.convert('RGB')
  tinput = mt(image)
  return tinput.unsqueeze(0)

# use the ImageFolder class from torchvision that accepts datasets structured by datax.py
# create datasets with appropriate transformation
image_datasets = {x: datasets.ImageFolder(os.path.join(DATA_DIR, x), data_transforms[x]) for x in data_transforms}
# create dataloaders
dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=BATCH_SIZE, shuffle=True, num_workers=WORKERS) for x in data_transforms}

dataset_sizes = {x: len(image_datasets[x]) for x in data_transforms}
class_names = image_datasets['test'].classes
number_classes = len(class_names)


# utility method for displaying batches images
def imshow(inp, title=None):
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array(IMAGE_NET_MEAN)
    std = np.array(IMAGE_NET_STD)
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(0.001)

# utility method for displaying layered images
def actshow(inp,activations, title=None):
    inp = torch.squeeze(inp,0)
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array(IMAGE_NET_MEAN)
    std = np.array(IMAGE_NET_STD)
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp, cmap='jet', alpha=1)
    plt.imshow(activations, cmap='jet', alpha=0.5)
    if title is not None:
        plt.title(title)
    plt.pause(0.001)

def show_activation_grid(original, activations, pclasses):
    inp = torch.squeeze(original,0)
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array(IMAGE_NET_MEAN)
    std = np.array(IMAGE_NET_STD)
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    # scale and collect activation maps to size of original image
    scaled = []
    for cam in activations:
        zcam = nd.zoom(cam, zoom=(32,32), order=1)
        scaled.append(zcam)
    # extend original image to x number of activations
    ginp = [inp] * len(activations)
    layer_one = torchvision.utils.make_grid(ginp)
    layer_two = torchvision.utils.make_grid(scaled)
    plt.imshow(layer_one, cmap='jet', alpha=1)
    plt.imshow(layer_two,cmap='jet', alpha=0.4)
    #plt.title(title=[pclasses[x] for x])
    plt.pause(0.001)

def show_sub_plots(original, activations, pclasses):
    f, axarr = plt.subplots(1,len(activations))
    inp = torch.squeeze(original,0)
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array(IMAGE_NET_MEAN)
    std = np.array(IMAGE_NET_STD)
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    # plot original image in all cells
    i = 0
    while i < len(activations):
        axarr[i].imshow(inp, cmap='jet', alpha=1)
        axarr[i].set_title(pclasses[i])
        i += 1
    # scale and collect activation maps to size of original image
    #scaled = []
    col = 0
    for cam in activations:
        zcam = nd.zoom(cam, zoom=(32,32), order=1)
        #scaled.append(zcam)
        axarr[col].imshow(zcam, cmap='seismic', alpha=0.4)
        col += 1
    # pause to let render
    plt.pause(0.001)


class LayerActivations():
    features=[]
    def __init__(self, model):
        self.hooks = []
        # Resnet layer/phase 4 is the last layer before GAP 
        self.hooks.append(model.layer4.register_forward_hook(self.hook_fn))

    def hook_fn(self, module, input, output):
        self.features.append(output)

    def remove(self):
        for hook in self.hooks:
            hook.remove()

# utility method to compute class activation maps 
def get_cam(model, outputs, activations, index=0):
    # get activations and remove the batch dimension
    out_features = activations.features[index].squeeze(0)
    # convert from [2048,7,7] to [7,7,2048]
    out_features = np.transpose(out_features.cpu().detach(), (1,2,0))
    # get probability distribution on the outputs
    probs = torch.nn.functional.softmax(outputs, dim=1)
    # get the axis of predicted class
    predicted = np.argmax(probs.cpu().detach())
    #print("PROBABILITIES: ", probs)
    # get all weights from GAP to FC
    aw = model.fc.weight
    # filter for predicted class
    fw = aw[predicted,:]
    # compute dot product between features and weights
    cam = np.dot(out_features.detach(), fw.detach().cpu())
    return cam, predicted

def comp_cam(a):
    return 1.0 - a

def threshhold_conv(a, threshold):
    if a >= threshold:
        return a + threshold
    else:
        return a




if __name__ == '__main__':

    covid_classes = ["COVID", "NORMAL"]
    #xray_classes = ["COVID", "NORMAL", "PNEUMONIA"]
    xray_classes = ["NORMAL", "PNEUMONIA"]

    IMAGE = "data/xcovid/test/COVID/4-x-day1.jpg"
    IMAGE2 = "data/xcovid/test/COVID/16660_2_1.jpg"
    IMAGE3 = "data/xcovid/test/COVID/covid-19-pneumonia-20.jpg"

    IMAGEX = "data/xray/test/PNEUMONIA/person1657_virus_2864.jpeg"
    #NORMAL
    IMAGE4 = "data/xcovid/test/NORMAL/IM-0009-0001.jpeg"
    IMAGE5 = "data/xray/test/PNEUMONIA/person1_virus_6.jpeg" # flaged as COVID
    IMAGE6 = "data/xray/test/PNEUMONIA/person172_bacteria_827.jpeg"
    # FALSE NEGATIVE
    IMAGE7 = "data/xray/test/PNEUMONIA/person1614_virus_2800.jpeg"
    # COVID=false, PNEU_viral=true
    IMAGE8 = "data/xray/test/PNEUMONIA/person1613_virus_2799.jpeg"

    IM1 = "data/xcovid/test/COVID/covid-19-pneumonia-12.jpg"
    IM2 = "data/xcovid/test/COVID/covid-19-pneumonia-15-PA.jpg"
    IM3 = "data/xcovid/test/COVID/covid-19-pneumonia-20.jpg"
    IM4 = "data/xcovid/test/COVID/covid-19-pneumonia-22-day2-pa.png"
    # IM5 NORMAL FOR PNEUMONIA ON BINARY EXPERT MODEL
    IM5 = "data/xcovid/test/COVID/covid-19-pneumonia-53.jpg" # covidpen
    IM6 = "data/xcovid/val/COVID/covid-19-pneumonia-14-PA.png"
    IM7 = "data/xcovid/test/COVID/auntminnie-d-2020_01_28_23_51_6665_2020_01_28_Vietnam_coronavirus.jpeg"
    IM8 = "data/xcovid/test/COVID/auntminnie-c-2020_01_28_23_51_6665_2020_01_28_Vietnam_coronavirus.jpeg"
    IM9 = "data/xcovid/test/COVID/lancet-case2b.jpg"

    #IM10 = "data/xcovid/COVID-19 (87).png"
    # IM11 = "data/xcovid/COVID-19 (83).png"
    # IM12 = "data/xcovid/COVID-19 (62).png"

    IM20 = "data/xcovid/test/COVID/lancet-case2b.jpg"
    IM21 = "data/xcovid/test/COVID/88de9d8c39e946abd495b37cd07d89e5-2ee6-0.jpg" #1 	2 	2 	1 	1 	2
    IM22 = "data/xcovid/test/COVID/88de9d8c39e946abd495b37cd07d89e5-6531-0.jpg" #0 	0 	3 	0 	0 	3 	
   
    # model one
    COVID_MODEL = 'models/resnetCovidNet.pt'
    model = torch.load(COVID_MODEL)
    model.eval()
    # activations for model one
    acts = LayerActivations(model)
    model.to(device)

    # model two
    XRAY = 'models/resnetXray.pt'
    #XRAY = 'models/resnetMULTIxray.pt'
    modelx = torch.load(XRAY)
    modelx.eval()
    actsx = LayerActivations(modelx)
    modelx.to(device)

    # now we feed an image to both of them
    with open(IMAGE3, 'rb') as f:
        image_bytes = f.read()
        
        inputs = transform_image(image_bytes).to(device)
        cams = []
        pclasses = []
        # get the first output
        output_one = model(inputs)
        cam_one, one_class = get_cam(model, output_one,acts,index=0)
        # print('PREDICTED: ', covid_classes[one_class])
        print("CAM 1: \n", cam_one)
        cams.append(cam_one)
        pclasses.append(covid_classes[one_class])

        output_two = modelx(inputs)
        cam_two, two_class = get_cam(modelx, output_two,actsx, index=0)
        # print('PREDICTED: ', xray_classes[two_class])
        print("CAM 2: \n", cam_two)
        cams.append(cam_two)
        pclasses.append(xray_classes[two_class])

        # append first predicted class for differences
        pclasses.append(covid_classes[one_class])

        #  their normalized difference (watch out for 0)
        max_one = cam_one.max()
        if max_one == 0.0:
            max_one = 1.0
        cam_one_norm = cam_one / max_one
        max_two = cam_two.max()
        if max_two == 0.0:
            max_two = 1.0
        cam_two_norm = cam_two / max_two
       
        # TRY DIFF NORM
        #cam_one_norm = minmax_scale(cam_one)
        print("NORM 1: ", cam_one_norm)
        #cam_two_norm = minmax_scale(cam_two)
        print("NORM 2: ", cam_two_norm)

        # record frobenius norm of both normalized CAMs
        fnorm_one = np.linalg.norm(cam_one_norm, 'fro')
        fnorm_two = np.linalg.norm(cam_two_norm, 'fro')
        print('F NORM ONE: ', fnorm_one)
        print('F NORM TWO: ', fnorm_two)
        diff = np.subtract(cam_one_norm,cam_two_norm)
        diff = np.around(diff, decimals=3)
        print("DIFF: ", diff)
        # scale by Y = e^m(dif_i,j)
        m = 5
        diff = diff * m
        print("scaled DIFF: \n", diff)
        # take the exponent e^scaled diff
        ediff = np.exp(diff)
        # exp diff
        print("exponent DIFF: \n", ediff)
        
        cams.append(ediff)

        show_sub_plots(inputs.cpu(),cams, pclasses)
        plt.show()
