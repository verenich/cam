# Artifact repository for Amplified Directed Divergence work

This work developed a method to utilize binary expert convolutional neural networks to extract class relevant regions of interest in environments where similar classes exhibit similar activations of their class activation maps.

![](samples/v8arch.jpg)

This repository contains sourcecode to reproduce results published in the conference proceedings shown below:

```bib
@inproceedings{ICMLA51294:verenich,
  title={{Improving Explainability of Image Classification in Scenarios with Class Overlap: Application to COVID-19 and Pneumonia}},
  author={Verenich, Edward and Velasquez, Alvaro and Khan, Nazar and Hussain, Faraz},
  booktitle = {19th IEEE International Conference on Machine Learning Applications},
    pages={1402-1409},
      doi={10.1109/ICMLA51294.2020.00218},
  year={2020}
}
```
## Main scripts to use:
After cloning the repository locally (with Pytorch installed), run the following to execute ADDk examples:
---
<code>camtest.py</code> use to perform improved localization of COVID-19 regions in supplied chest X-ray imagery.
---
<code>camwheel.py</code> use to localize a car wheel (CARWHEEL) from the car class.
---
<code>camplot.py</code> use to generate sample Class Activation Maps for weakly supervised localization.
---

## COVID-19 Radiology examples
The first image of each row represents a Class Activation Map (CAM) produced for COVID-19, the second shows a CAM produced for the Pneumonia class, and the third image represents the output of the ADD kernel method to extract COVID-19 relevant features.

![](samples/COVID_1.png)
![](samples/COVID_2.png)
![](samples/COVID_8.png)
![](samples/IM9_COVID.png)
![](samples/IM21_COVID.png)
![](samples/IMAGE3_COVID.png)

## Natural imagery exampels, (CARWHEEL vs CAR)
In these examples we are trying to better localize the CARWHEEL class that overlaps with the CAR class, the third image in each row shows the output of the ADD kernel method.
![](samples/Figure_1.png)
![](samples/Figure_2.png)
![](samples/Figure_4.png)

## Radiology data
Radiology data used to train binary expert models for COVID-19 and Pneumonia can be found here:
https://github.com/ieee8023/covid-chestxray-dataset

Please note that authors of this repository may have updated it with new samples. We provide a sample script <code>covidata.py</code> to extract COVID-19 images with Posterior-Anterior (PA) view of the patient. Adjust the script as need to create new datasets.

## Natural imagery data
Natural imagery examples are public use images from Google Search for cars.

## Data preparation
All data is normalized using ImageNet's mean and s.t.d. values:
```python
IMAGE_NET_MEAN = [0.485, 0.456, 0.406]
IMAGE_NET_STD = [0.229, 0.224, 0.225]
```
To create new train, validate, and test partitions use <code>datax.py</code> for desired splits, or <code>datax_m.py</code> to create multiple folds for _K_ fold validation.
