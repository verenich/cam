'''
This code finds all images of patients of a specified VIRUS and X-Ray view and stores selected image to an OUTPUT directory
+ It uses metadata.csv for searching and retrieving images name
+ Using ./images folder it selects the retrieved images and copies them in output folder
Code can be modified for any combination of selection of images
original script: https://github.com/ieee8023/covid-chestxray-dataset
modified to output covid cases to an output directory for further processing with datax.py
'''

import pandas as pd
import argparse
import shutil
import os

# Selecting all combination of 'COVID-19' patients with 'PA' X-Ray view
virus = "COVID-19" # Virus to look for
x_ray_view = "PA" # View of X-Ray

metadata = "metadata.csv" # Meta info
imageDir = "images" # Directory of images
outputDir = 'covidcases' # Output directory to store selected images

metadata_csv = pd.read_csv(metadata)

# loop over the rows of the COVID-19 data frame
for (i, row) in metadata_csv.iterrows():
  if row["finding"] != virus or row["view"] != x_ray_view:
    continue
  filename = row["filename"].split(os.path.sep)[-1]
  print("filename: ", filename)
  #outputPath = os.path.sep.join([outputDir, filename])
  #shutil.copy(os.path.join(class_map[class_key], f), os.path.join(OUTPUT_DIR,d_type,class_key,f))
  shutil.copy(os.path.join(imageDir,filename), os.path.join(outputDir, filename))