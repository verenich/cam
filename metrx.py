# The modlue is used to calculate performance metrics on a confusion 
# matrix passed in as a torch.FloatTensor
# Developed with Python 3.7.3

import torch

class Error(Exception):
   """Base class for other exceptions"""
   pass
class NotTensor(Error):
   """Raised when input is not a torch.tensor"""
   pass


def accuracy(matrix):
  if matrix.type() == 'torch.FloatTensor':
    acc = torch.sum(matrix.diag()) / torch.sum(matrix)
    return acc
  else:
    raise NotTensor

def per_class_accuracy(matrix):

  if matrix.type() == 'torch.FloatTensor':
    acc = matrix.diag() / matrix.sum(1)
    return acc
    
  else:
    raise NotTensor

def combinedF1(matrix):
  if matrix.type() == 'torch.FloatTensor':
    # all true positives
    TP = torch.sum(matrix.diag())
    # subtract TP from sum of all elements
    ERRORS = torch.sum(matrix) - TP
    # F1 = 2TP / 2TP + FP + FN
    return (2*TP / (2*TP + ERRORS))
  else:
    raise NotTensor

def sensitivity(matrix, class_index):
  if matrix.type() == 'torch.FloatTensor':
    NUM = matrix[class_index, class_index]
    DEN = torch.sum(matrix[:,class_index])
    return NUM / DEN
  else:
    raise NotTensor

def combinedSensitivity(matrix):
  if matrix.type() == 'torch.FloatTensor':
    nclasses = matrix.size()[0]
    total = 0.0
    i = 0
    while i < nclasses:
      total += sensitivity(matrix, i)
      i += 1
    return total / nclasses
  else:
    raise NotTensor 


def precision(matrix, class_index):
  if matrix.type() == 'torch.FloatTensor':
    NUM = matrix[class_index, class_index]
    DEN = torch.sum(matrix[class_index,:])
    return NUM / DEN
  else:
    raise NotTensor

def combinedPrecision(matrix):
  if matrix.type() == 'torch.FloatTensor':
    nclasses = matrix.size()[0]
    total = 0.0
    i = 0
    while i < nclasses:
      total += precision(matrix, i)
      i += 1
    return total / nclasses
  else:
    raise NotTensor 
    

def number_false_negative(matrix, class_index):
  if matrix.type() == 'torch.FloatTensor':
    return torch.sum(matrix[:,class_index]) - matrix[class_index, class_index]
  else:
    raise NotTensor

def number_false_positive(matrix, class_index):
  if matrix.type() == 'torch.FloatTensor':
    return torch.sum(matrix[class_index,:]) - matrix[class_index, class_index]
  else:
    raise NotTensor

def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)
