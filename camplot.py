# Copyright <2019> Edward Verenich <verenie@clarkson.edu>
# MIT license <https://opensource.org/licenses/MIT>

from __future__ import print_function, division

import torch
import torch.nn as nn
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import time
import os
import copy
import csv
import metrx as mx
import transformsx
import scipy.ndimage as nd



# set the data directory for training models
DATA_DIR = 'data/xcovid'
BATCH_SIZE = 1
WORKERS = 2
IMAGE_NET_MEAN = [0.485, 0.456, 0.406]
IMAGE_NET_STD = [0.229, 0.224, 0.225]

# check for cuda and set gpu or cpu
device = torch.device("cuda:0" if torch.cuda.is_available() else 'cpu')

# data processing, adding additional sets and transforms, i.e. test corresponds to a directory
# and will create appropriate datasets
data_transforms = {
    'test': transforms.Compose([
        transforms.Resize(224),
        transforms.CenterCrop(224),
        #transformsx.GausNoise(10),
        transforms.ToTensor(),
        transforms.Normalize(IMAGE_NET_MEAN, IMAGE_NET_STD)
    ])
}

# use the ImageFolder class from torchvision that accepts datasets structured by datax.py
# create datasets with appropriate transformation
image_datasets = {x: datasets.ImageFolder(os.path.join(DATA_DIR, x), data_transforms[x]) for x in data_transforms}
# create dataloaders
dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=BATCH_SIZE, shuffle=True, num_workers=WORKERS) for x in data_transforms}

dataset_sizes = {x: len(image_datasets[x]) for x in data_transforms}
class_names = image_datasets['test'].classes
number_classes = len(class_names)


# utility method for displaying batches images
def imshow(inp, title=None):
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array(IMAGE_NET_MEAN)
    std = np.array(IMAGE_NET_STD)
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(0.001)

# utility method for displaying batches images
def actshow(inp,activations, title=None):
    inp = torch.squeeze(inp,0)
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array(IMAGE_NET_MEAN)
    std = np.array(IMAGE_NET_STD)
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp, cmap='jet', alpha=1)
    plt.imshow(activations, cmap='jet', alpha=0.5)
    if title is not None:
        plt.title(title)
    plt.pause(0.001)


class LayerActivations():
    features=[]
    def __init__(self, model):
        self.hooks = []
        # Resnet layer/phase 4 is the last layer before GAP 
        self.hooks.append(model.layer4.register_forward_hook(self.hook_fn))

    def hook_fn(self, module, input, output):
        self.features.append(output)

    def remove(self):
        for hook in self.hooks:
            hook.remove()

if __name__ == '__main__':
   
    # specify model to test
    MODEL_NAME = 'models/resnetCovidNet.pt'
    model = torch.load(MODEL_NAME)
    model.eval()
    # activations
    acts = LayerActivations(model)

    model.to(device)
  

    
    # loop through data
    index = 0
    for inputs, labels in dataloaders['test']:
        inputs = inputs.to(device)
        labels = labels.to(device)
        #acts = acts.to(device)
        # keep track of gradients for training only
        with torch.set_grad_enabled(False):
            # forward pass
            outputs = model(inputs)
            out_features = acts.features[index].squeeze(0)
            out_features = np.transpose(out_features.cpu(), (1,2,0))
            #print("Outputs: ", outputs)
            # send ouputs through a softmax function
            probs = torch.nn.functional.softmax(outputs, dim=1)
            _, predictions = torch.max(outputs, 1)
            # axis of predicted class
            pred = np.argmax(probs.cpu().detach())
            # obtain all the weights connecting GAP to final fully connected layer
            W = model.fc.weight
            # get the weights associated with predicted class
            w = W[pred,:]
            # dot prodcut between  features and weights
            cam = np.dot(out_features.detach(), w.detach().cpu())
            print("CAM: ", cam)
            # scale class activations with scipy
            class_activation = nd.zoom(cam, zoom=(32,32), order=1)


            print("Probs: ", probs)
            print("Labels: ",labels)


            # plot image and activation map
            actshow(inputs.cpu(),class_activation, class_names[pred])
            plt.show()
            # increment the index
            index = index + 1
           